<?php session_start();
	
	// var_dump($_SESSION);
	require_once '../controllers/connection.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

	<title> QStore | <?php echo getTitle(); ?> </title>
	
	<!-- animate css -->
	

	<!-- fontawesome icons -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Grenze|Zhi+Mang+Xing&display=swap" rel="stylesheet">
 

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- external css -->
	<link rel="stylesheet" href="../assets/css/style.css">
	


</head>
<body>

	<nav class="navbar navbar-expand-lg">
		<h4 class="text-success">Desserts Hub</h4>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-nav">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div id="navbar-nav" class="collapse navbar-collapse" >
			<ul class="navbar-nav ml-auto">
				<?php 
					// isset() function checks if a variable/element has been assigned a value
					// if session email has no assigned value/has not been set. display register and 
					// login in the navbar.
					// else, display the lagout button

						// var_dump($_SESSION['user']['role_id']);// returns the value of the username field of the current owner of $_SESSION['firstname']
						// var_dump($_SESSION['user']['email']);
						// var_dump($_SESSION['user']['password']);
						// var_dump($_SESSION);
					if(isset($_SESSION['user']['firstname'])) {
						$username = $_SESSION['user']['firstname'];
						// var_dump($username);
					}
				
					// isset function checks if there is an assigned value for the variable/element
					if(!isset($_SESSION['user']) || $_SESSION['user']['role_id'] != 1) {
						echo "";
					}else{
						echo"
							<li class='nav-item'>
								<a class='nav-link' href='./add_product.php'> Add Product </a>
							</li>
						";
					} 

					if(!isset($_SESSION['user']['username'])) {
						echo "
							<li class='nav-item px-3 '>
								<a class='nav-link' href='./register.php'>Register</a>
							</li>

							<li class='nav-item px-3'>
								<a class='nav-link' href='./login.php'>Login </a>
							</li>

							<li class='nav-item px-3 text-primary'>
								<a class='nav-link'> Welcome,Guest </a>
							</li>
						";
					} 
					else {
						// meaning someone is logged in
							echo "

								 <li class='nav-item'>
									<a class='nav-link' href='./cart.php'> 
									Cart</a>
								</li>

								 <li class='nav-item'>
									<a class='nav-link' href='./gallery.php'> 
									Gallery</a>
								</li>

								<li class='nav-item px-3'>
									<a class='nav-link' href='./logout.php'>Logout </a>
								</li>

								<li class='nav-item text-primary'>
									<a class='nav-link'> Hello $username </a>
								</li>
							";
					}
				 ?>
			</ul>
		</div>
	</nav>