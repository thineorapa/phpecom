<?php 

	require_once '../partials/header.php';

	function getTitle() {
		return "Add Item Page";
	}

 ?>


 	<div class="container-fluid col-md-8 offset-md-2 pb-5">
 		<h2 class="text-center text-success">Add New Item</h2>

 		<div class="row">
 			<div class="col-md-8 mx-auto">
 				<form action="../controllers/add_item.php" method="POST" enctype="multipart/form-data">
 					<div class="form-group">
 						<label for="productName">Product Name</label>
 						<input type="text" id="productName" class="form-control" name="productName">
 					</div>

 					<div class="form-group">
 						<label for="price">Price</label>
 						<input type="number" id="price" class="form-control" name="price">
 					</div>


 					<div class="form-group">
 						<label for="description">Description</label>
 						<input type="text" id="description" class="form-control" name="description">
 					</div>


 					<div class="form-group">
 						<label for="image">image</label>
 						<input type="file" id="image" class="form-control" name="image">
 					</div>

 					<button type="submit" class="btn btn-success">Add New Item</button>
 				</form>
 			</div><!--  end col -->
 		</div> <!-- end row -->
 	</div> <!-- end container -->
 	
 <?php
	require_once '../partials/footer.php';

?>