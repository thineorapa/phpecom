<?php 
	session_start();

	// to remove all session variables
	session_unset();

	// destroy the session
	session_destroy();

	// redirect to login page
	header('Location: ./login.php');


 ?>