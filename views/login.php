<?php

	require_once "../partials/header.php";

	function getTitle() {
		return "Login Page";
	}

?>


	<div class="container-fluid col-md-8 offset-md-2 log">
		<h2 class="text-center text-success mt-5 pt-3">Login Page</h2>

		<div class="row">
			<div class="col-md-6 mx-auto">
				<form action="../controllers/authenticate.php" method="POST">

					<div class="form-group">
						<label for="username">Username</label>
						<input type="text" id="username" name="username" class="form-control">
					</div>

					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" id="password" name="password" class="form-control">
					</div>

					<button type="submit" class="btn btn-success btn-block mt-5">Login</button>
				</form>
			</div>
		</div>
	</div>
 
<?php 
	require_once "../partials/footer.php";
 ?>