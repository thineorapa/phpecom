<?php
	// the require statement is the same as the include statement in the
	// sense that it allows devs to reference files from a different location. 
	// the only difference is the  error handling.
	// include will throw  a warning but still execute the code while require will
	// throw a fatal error. include_once and require_once does the same thing
	// but once the file is included/required already, it will not execute  again

	// include '../partials/header.php';
	// include_once '../partials/header.php';
	require_once '../partials/header.php';

	function getTitle() {
		return "Register Page";
	}
?>
	<div class="container-fluid col-md-8 offset-md-2 pb-5">
		<h2 class="text-center text-success pt-3">Registration Page</h2>
		<div class="row">
			<div class="col-md-6 mx-auto">
				<!-- the action attribute sets the destination to which the form data is submitted.
				the value of this can be an absolute or relative url -->

				<!-- the method attribute specifies the type of HTTP request you want to make when sending the form data -->

				<form action="../controllers/register_user.php" method="POST">
					<div class="form-group">
						<label for="fname">First Name</label>
						<input type="text" id="fname" name="firstName" class="form-control" required>
					</div>

					<div class="form-group">
						<label for="lname">Last Name</label>
						<input type="text" id="fname" name="lastName" class="form-control" required>
					</div>
						
					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" id="address" name="address" class="form-control" required>
					</div>

					<div class="form-group">
						<label for="username">Username</label>
						<input type="text" id="username" name="username" class="form-control" required>
					</div>

					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" id="email" name="email" class="form-control" required>
					</div>

					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" id="password" name="password" class="form-control" required>
					</div>

					<div class="form-group">
						<label for="confirm">Confirm Password</label>
						<input type="password" id="confirm" name="confirmPassword" class="form-control" required>
					</div>
					<!-- type submit will submit the form -->
					<button type="submit" class="btn btn-success btn-block mt-5">Register</button>
				</form>
			</div>
		</div>
		
	</div> <!-- end of container-->
    
    
<?php
    require "../partials/footer.php";
?>
    
    
