<?php
/*
inorder for us to acess the stored data in the $_SESSION across different pages,
we just initialize the session with the session start function
syntax: session_start()
*/

/*
once we have initilize the session. we can then freely access all the session variables
and its values stored in the current $_SESSION and use it as we please
*/

/*
if($_SESSION['email'] == "admin@email.com") {
	echo "Hello admin";
}
*/

	require_once '../partials/header.php';

	function getTitle() {
		return 'Gallery Page';
	}

?>

	<div class="container">
		<h2 class="text-center text-success">Products Dashboard</h2>
		<div class="row">
			<div class="col-md-4 offset-md-9 my-3 text-center">
				<h5 class="text-success"> Sort By: </h5>
				<ul>
					<li class="item">

						<a href="../controllers/sort.php?sort=asc">Price(Lowest to Highest)</a>
					</li>

					<li class="item">
						<a href="../controllers/sort.php?sort=desc">Price(Highest to Lowest)</a>
					</li>
				</ul>
			</div>
			<?php

				$product_query = "SELECT * FROM items";
				// var_dump($product_query);

				// sorting
				if(isset($_SESSION['sort'])) {
					// order by price ASC
					// select * from items order by price asc
					$product_query .= $_SESSION['sort'];
				}

				// mysqli_query() performs a query to our db that returns true or false
				// syntax: mysqli_query(connection, query to execute)
				$products_array = mysqli_query($conn, $product_query);
				// var_dump($products_array);

				// iterate the array
				foreach($products_array as $product) {
					// var_dump($product);


			?>

			<div class="col-md-4">
				<div class="card">
					<img src="<?= $product['image']; ?>" class="card-img-top">

					<div class="card-body">
						<h5 class="card-title text-success"><?= $product['name']; ?></h5>
						<p class="card-text">Price: <?= $product['price']; ?></p>
						<p class="card-text">Description: <?= $product['description']; ?></p>
					</div> <!-- end card body -->

					<div class="card-footer">
						<form  action="../controllers/update_cart.php" method="POST">
							<input type="number" name="item_quantity" class="form-control text-center" min="1" value="1">
							<input type="hidden" name="item_id" value=<?= $product['id']; ?>>
							<button class="btn btn-block btn-success add-to-cart">Add To Cart</button>
						</form>
						<a href="../controllers/delete_item.php?id=<?= $product['id'];?>" class="btn btn-danger btn-block">Delete Item</a>
						<a href="./edit_form.php?id=<?= $product['id']; ?>" class="btn btn-primary btn-block">Update Item</a>
					</div>

				</div> <!-- end card -->
			</div><!--  end cols -->
		<?php } ?>
		</div> <!-- end row -->
	</div> <!-- end container -->

<?php
	require_once '../partials/footer.php';

?>