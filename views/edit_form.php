<?php 

	require_once '../partials/header.php';

	function getTitle() {
		return 'Update details';
	}

	$item_to_be_edited = $_GET['id'];

	// var_dump($item_to_be_edited);
	// query that retrieves the item form the items table where the value of the id column matches the value of $item_to_be_edited
	$item_query = "SELECT name, price, description FROM items WHERE id = $item_to_be_edited";
	$item_result = mysqli_query($conn, $item_query);
	$item = mysqli_fetch_assoc($item_result);
	// var_dump($item);


 ?>

 <div class="container">
 	<h2 class="text-center">Update item</h2>
 	<div class="row">
 		<div class="col-md-10 mx-auto">
 			<form action="" method="POST">
 				<label for="">Item name:</label>
 				<input type="text" name="item_name" class="form-control" value="<?php echo $item['name']; ?>">

 				<label for="">Item price:</label>
 				<input type="text" name="item_price" class="form-control" value="<?php echo $item['price']; ?>">

 				<label for="">Description:</label>
 				<textarea name="description" id="" cols="60" rows="10">"<?php echo $item['description']; ?>"</textarea>

 				<button class="btn btn-primary">Update</button>

 			</form>
 		</div>
 	</div>
 </div>

 <?php 
 	require_once '../partials/footer.php';
  ?>