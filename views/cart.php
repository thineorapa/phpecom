<?php

	require_once '../partials/header.php';

	function getTitle() {
		return 'Items in cart';
	}

?>


<div class="container-fluid col-md-10 pb-5">
	<h2 class="text-center text-success mb-5 pt-3">My Cart</h2>
	<div class="row">
		<div class="col-md-10 offset-md-1 max-auto">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="cart-items">
					<thead>
						<tr>
							<th>Item</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php 
							// var_dump($_SESSION['cart']);
							//if cart has an item
							if(isset($_SESSION['cart']) && count($_SESSION['cart']) != 0) {

							$total = 0;
							foreach($_SESSION['cart'] as $item_id => $item_quantity) {
								// var_dump($item_quantity);
								$item_qry = "SELECT * FROM items WHERE id = $item_id";
								$result = mysqli_query($conn, $item_qry);
								$indiv_item = mysqli_fetch_assoc($result);

								// var_dump($indiv_item);

								// convert the assoc arrayinto set of variables with associative
								// array keys as the variable names
								extract($indiv_item);

								$subtotal = $price * $item_quantity;

								$total += $subtotal;

							// echo $name;
						?>
							<tr>
								<td><?= $name; ?><img class="imgCart" src="<?= $image;?>"></td>
								<td><?= $price; ?></td>
								<!-- <td><?= $description; ?></td> -->
								<td>
									<form action="../controllers/update_cart.php" method="POST">
										<input type="hidden" name="item_id" min="1" value="<?= $item_id;?>">
										<input type="number" name="item_quantity" min="1" class="quantityInput" value=<?= $item_quantity; ?>>
										<input type="hidden" name="updateFromCart" value="true">
										<button class="btn btn-outline-primary mt-1">Update quantity</button>
									</form>
								</td>
								<!-- number format puts 2 decimals -->
								<td><?= number_format($subtotal, 2); ?></td>
								<td>
									<form action="../controllers/remove_from_cart.php" method="POST">
									<input type="hidden" name="item_id" value="<?= $item_id;?>">
									<input type="hidden" name="item_id" value="<?= $id?>">
										<button class="btn-danger btn btn-block">Remove From Cart</button>
									</form>
								</td>
							</tr>
						<?php
							} // end of for each
						?>
							
							<tr>
								<td></td>
								<td></td>
								<td>Total: <?= number_format($total, 2) ; ?></td>
								<td><a href="../controllers/checkout.php" class="btn btn-primary btn-block">Checkout</a></td>
								<td><a href="../controllers/empty_cart.php" class="btn btn-outline-danger btn-block">Empty Cart</a></td>
							</tr>

						<?php  
							}else {
								echo "
									<tr>
										<td class='text-center' colspan='6'> No items in the cart</td>
									</tr>
								";
							}
						?>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php

	require_once '../partials/footer.php';
?>