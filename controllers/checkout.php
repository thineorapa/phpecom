<?php 

	session_start();

	//when to checkout?
	//1.sure of the cart content
	//2.user logged in

	require_once './connection.php';

	// this makes sure that the user is logged in
	if(isset($_SESSION['user'])) {
		//orders table 
		//user id, transactioncode, purchase date, total, status id, payment mode id

		//gets the user id from SESSION user
		$user_id = $_SESSION['user']['id'];
		$transaction_code = "DessertsHub" . time(); //time sets as a reference number
		$total = 0;/*update this after we compute the total from the cart table*/
		$status_id = 1; /*default value for pending*/
		$payment_mode_id = 1; /*default value for COD*/

		// var_dump($transaction_code);

		$order_query = "INSERT INTO orders(user_id, transaction_code, total, status_id, payment_mode_id) VALUES('$user_id', '$transaction_code', '$total', '$status_id', '$payment_mode_id')";

		// var_dump($order_query);
		$order_result = mysqli_query($conn, $order_query);

		// mysqli_insert_id() -> returns the id of the most recent order
		$order_id = mysqli_insert_id($conn);

		// var_dump($order_id);
		// create entries for our item_order table
		// item_id(Session cart), recent order_id($order_id)
		// var_dump($_SESSION['cart']);

		foreach ($_SESSION['cart'] as $item_id => $quantity) {
			$item_query = "SELECT * FROM items WHERE id = $item_id";
			$item_result = mysqli_query($conn, $item_query);
			$item = mysqli_fetch_assoc($item_result);
			// var_dump($item);

			$total += $item['price'] * $quantity;

			//insert data to item_order table
			// order_id, item_id, & quantity
			$item_order_query = "INSERT INTO item_order(order_id, item_id, quantity) VALUES('$order_id', '$item_id', '$quantity')";

			$item_order_result = mysqli_query($conn, $item_order_query);

			// var_dump($item_order_result);
		}
		// update orders table to reflect the correct total
		//  syntax: UPDATE table_name SET column_name = $new_value WHERE column_name = $order_id;
		$update_total_orders = "UPDATE orders SET total = $total WHERE id = $order_id";
		$update_total_result = mysqli_query($conn, $update_total_orders);

		header('Location: ../views/gallery.php');
	}



 ?>