<?php
	require_once './connection.php';

	// PHP has predefined variables which are designed to collect data sent by HTML form
	// which $_POST and $_GET superglobal variables.

	// superglobal variables simply means that it is a specially pre-defined
	// variable(normally arrays) that can be accessed in the program.

	// the request returned an array where it established the name attribute of the input 
	// as the key and the value inputted to it as its value

	// var_dump($_POST);
	// var_dump($_POST['firstName']);
	// var_dump($_GET);

	//they($_POST and $_GET) do the same thing as both variables handle html form
    // data but the main difference is when you use the GET method, the string we
    // entered in the form will be displayed in the URL. POST, on the other hand, sends forms behind the scenes.
    // thus, not seeing the form data in the URL. 
	
	// sanitize our inputs
	$fname = htmlspecialchars($_POST ['firstName']);
	$lname = htmlspecialchars($_POST ['lastName']);
	$username = htmlspecialchars($_POST ['username']);
	$address = htmlspecialchars($_POST ['address']);
	$email = htmlspecialchars($_POST ['email']);
	$password = htmlspecialchars($_POST ['password']);
	$confirmPassword = htmlspecialchars($_POST ['confirmPassword']);
	$role_id = 2;

	// $fname = "thine";
	// $lname = "orapa";
	// $username = "thineorapa";
	// $address = "Caloocan City";
	// $email = "thineorapa@gmail.com";
	// $password = "admin123";
	// $confirmPassword = "admin123";
	// $role_id = 1;


	// check the values of the variables tha we used to store the user credentials
	// var_dump($fname);
	// var_dump($lname);
	// var_dump($email);
	// var_dump($password);
	// var_dump($confirmPassword);

	if($password != "" || $confirmPassword != "") {
		// lets hash our password to make it secure
		$password = sha1($password);
		$confirmPassword = sha1($confirmPassword);
		// var_dump($password);
		// var_dump($confirmPassword);
		// check if $password is equal to $confirmPassword
		if($password === $confirmPassword) {
			// echo "password match";
		}else {
			echo "password did not match";
		}

		$insert_query = "INSERT INTO users(username, password, firstname, lastname, email, address, role_id) VALUES('$username', '$password', '$fname', '$lname', '$email', '$address', $role_id)";

		$result = mysqli_query($conn, $insert_query);

		if($result) {
			echo 'Registered Succesfully...';
		}
		else {
			echo mysqli_error($conn);
		}
		
		header('Location: ../views/login.php');

	}else {
		echo "Please check the password fields";
	}

	if($fname != "" && $lname != "") {
		echo "<br>Welcome" ." " .  $fname . " " . $lname;

	}else {
		echo "Please provide a complete name";
	}

	// if email input is not equal to an empty string, echo out
	// your email is: (value of the email variable)
	// else, echo out please provide an email
	if($email != "") {
		echo "<br>your email is: " . $email;
	}else {
		echo "<br>Please provide an email";
	}


?>