	<?php 
	  
	 require_once './connection.php';

	// sanitize the inputs
	$productName = htmlspecialchars($_POST['productName']);
	$price = htmlspecialchars($_POST['price']);
	$desc = htmlspecialchars($_POST['description']);
	$category_id = 1;
	// check how superglobal $_FILES look
	// superglobal $_FILES is an assoc array that will contain a key
	// equivalent to the name given in our file input in the form which has an assoc array
	// of information of the uploaded file as its value

	// check how $_FILES['image'] look
	// ['image'] is the value of our name attribute for our input file
	// $_FILES['image'] will return all the details of the uploaded file in our web server
	// syntax $FILES['name in the form']['property key']
	// var_dump($_FILES['image']);
	// var_dump($_FILES['image']['name']);

	$filename = $_FILES['image']['name'];
	$filesize = $_FILES['image']['size'];
	$file_tmpname = $_FILES['image']['tmp_name'];

	// var_dump($filename);
	// var_dump($filesize);
	// var_dump($file_tmpname);


	// get the file extension of $filename using the pathinfo() and convert
	// it to lowercase chars.
	// pathinfo() will return an assoc array of information regarding the path 
	// and the file type of the uploaded file
	// w are uisng the PATHINFO_EXTENSION to only return the file extension
	// syntax: pathinfo(file to be checked, option)
	$file_type = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
	// var_dump($file_type);

	$isImg = false;
	$hasDetails = false;

	if($productName != "" && $price > 0 && $desc != "") {
		$hasDetails = true;
		// var_dump($hasDetails);
	}

	if($file_type == "jpg" || $file_type == "jpeg" || $file_type == "png" || $file_type == "gif" || $file_type == "svg") {

		$isImg = true;
		// var_dump($isImg);
	}

	if($filesize > 0 && $isImg && $hasDetails) {
		// echo "ready to upload";
		// lets declare the final path that we want to assign to the uploaded file
		$final_filepath = "../assets/images/" . $filename;
		// var_dump($final_filepath);

		// move the image that is temporarily stored in our server to the final path
		// syntax: move_uploaded_file(temporary_path, new_path)
		move_uploaded_file($file_tmpname, $final_filepath);
	}else {
		echo "please upload an image";
	}

	// create a new assoc array containing the product details
	$newProduct = ["name" => $productName, "price" => $price, "description" => $desc, "images" => $final_filepath];
	// var_dump($newProduct);

	// return contents of products.json in a string
	// $prod = file_get_contents("../assets/lib/products.json");
	// convert to a php array
	// $prods = json_decode($prod, true);
	// push the contents of $newProduct to the array
	// array_push($prods, $newProduct);
	// open the products.json file for writing
	// $to_write = fopen('../assets/lib/products.json', 'w');
	// write on the opened file using fwrite()
	// $encode = json_encode($prods, JSON_PRETTY_PRINT);
		// var_dump($encode);

	// fwrite($to_write, $encode);

	// close to opened file
	// fclose($to_write);

	$new_item_query = "INSERT INTO items(name, price, description, image, category_id) VALUES('$productName', $price, '$desc', '$final_filepath', $category_id)";
	$result = mysqli_query($conn, $new_item_query);

	if($result) {
		echo 'added the item successfully';
	}else {
		echo mysqli_error($conn);
	}

	//  redirect to the gallery
	header('Location: ../views/gallery.php');

				
 ?>