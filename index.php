<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
	<?php
	// php stands for php:hypertext processor
	// it allows developers to take what used to be static HTML
	// content and make it responsive to user's requests, or do the same 
	// with permanently stored data that resides in a database

	// to serve a project:
	// php -S address:port
	// php -S localhost:8000

	// to make our web server accessible by remote machines, serve it using 0.0.0.0
	// php -S 0.0.0.0:3000
	echo "hello";

	// the header function will ridirect to the indicated path.
	header('Location: ./views/register.php')

	?>

</body>
</html>